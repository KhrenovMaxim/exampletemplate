var gulp       = require('gulp'),
    sass         = require('gulp-sass'),
    browserSync  = require('browser-sync'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglifyjs'),
    cssnano      = require('gulp-cssnano'),
    rename       = require('gulp-rename'),
    del          = require('del'),
    imagemin     = require('gulp-imagemin'),
    pngquant     = require('imagemin-pngquant'),
    cache        = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    jade = require('gulp-jade');

gulp.task('sass', function() {
    return gulp.src('projectVeeva/app/templates/**/*.sass')
        .pipe(sass({outputStyle: 'nested'}))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
        .pipe(gulp.dest('projectVeeva/app/')) // Выгружаем результата в папку app/css
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

gulp.task('sassMain', function() {
    return gulp.src('projectVeeva/app/sass/main.sass')
        .pipe(sass({outputStyle: 'nested'}))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
        .pipe(gulp.dest('projectVeeva/app/pages/shared/css/')) // Выгружаем результата в папку app/css
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});


gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'projectVeeva/app' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});

gulp.task('scriptsMain', function() {
    return gulp.src([ // Берем все необходимые библиотеки
        'projectVeeva/app/js/*.js', // Берем jQuery
    ])
        .pipe(concat('main.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('projectVeeva/app/pages/shared/js/')); // Выгружаем в папку app/js
});


// gulp.task('sass', function() {
//     return gulp.src('projectVeeva/app/templates/**/*.sass')
//         .pipe(sass())
//         .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
//         .pipe(gulp.dest('projectVeeva/app')) // Выгружаем результата в папку app/css
//         .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
// });


gulp.task('scriptsSlide', function() {
    return gulp.src([ // Берем все необходимые библиотеки
        'projectVeeva/app/templates/**/*.js', // Берем jQuery
    ])
        .pipe(gulp.dest('projectVeeva/app')); // Выгружаем в папку app/js
});

gulp.task('scripts', function() {
    return gulp.src([ // Берем все необходимые библиотеки
        'projectVeeva/app/libs/jquery/dist/jquery.min.js', // Берем jQuery
    ])
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('projectVeeva/app/pages/shared/js/')); // Выгружаем в папку app/js
});

gulp.task('code', function() {
    return gulp.src('projectVeeva/app/*.html')
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('css-libs', function() {
    return gulp.src('projectVeeva/app/css/*.css') // Выбираем файл для минификации
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(cssnano()) // Сжимаем
        .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest('projectVeeva/app/pages/shared/css/')); // Выгружаем в папку app/css
});

gulp.task('clean', async function() {
    return del.sync('projectVeeva/dist'); // Удаляем папку dist перед сборкой
});



// gulp.task('scriptsSlide', function() {
//     return gulp.src([ // Берем все необходимые библиотеки
//         'projectVeeva/app/templates/**/*.js', // Берем jQuery
//     ])
//         .pipe(gulp.dest('projectVeeva/app')); // Выгружаем в папку app/js
// });



gulp.task('img', function() {
    return gulp.src('projectVeeva/app/templates/**/*.+(jpg|png|gif)') // Берем все изображения из app
        .pipe(cache(imagemin({ // С кешированием
            // .pipe(imagemin({ // Сжимаем изображения без кеширования
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))/**/)
        .pipe(gulp.dest('projectVeeva/app/')); // Выгружаем на продакшен
});

gulp.task('prebuild', async function() {

    var buildCss = gulp.src([ // Переносим библиотеки в продакшен
        'projectVeeva/dist/css/main.css',
        'projectVeeva/dist/css/libs.min.css'
    ])
        .pipe(gulp.dest('dist/css'))

    var buildFonts = gulp.src('projectVeeva/app/fonts/**/*') // Переносим шрифты в продакшен
        .pipe(gulp.dest('projectVeeva/dist/fonts'))

    var buildJs = gulp.src('projectVeeva/app/js/**/*') // Переносим скрипты в продакшен
        .pipe(gulp.dest('projectVeeva/dist/js'))

    var buildHtml = gulp.src('projectVeeva/app/*.html') // Переносим HTML в продакшен
        .pipe(gulp.dest('projectVeeva/dist'));

});


gulp.task('jade', function() {
    return gulp.src('projectVeeva/app/templates/**/*.jade')
        // .pipe(jade({client: true}))
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('projectVeeva/app/'));
});


gulp.task('fontsMain', function() {
    return gulp.src('projectVeeva/app/fonts/**/*')
        .pipe(gulp.dest('projectVeeva/app/pages/shared/fonts'));
});

gulp.task('clear', function (callback) {
    return cache.clearAll();
});

gulp.task('watch', function() {
    gulp.watch('projectVeeva/app/templates/pages/**/*.jade',gulp.parallel('jade'));
    gulp.watch('projectVeeva/app/templates/pages/**/*.sass', gulp.parallel('sass')); // Наблюдение за sass файлами
    gulp.watch('projectVeeva/app/sass/**/*.sass', gulp.parallel('sassMain')); // Наблюдение за sass файлами
    gulp.watch('projectVeeva/app/templates/pages/**/*.js', gulp.parallel('scriptsSlide')); // Наблюдение за sass файлами
    gulp.watch('projectVeeva/app/templates/pages/**/*.+(jpg|png|gif)', gulp.parallel('img')); // Наблюдение за sass файлами
    gulp.watch('projectVeeva/app/*.html', gulp.parallel('code')); // Наблюдение за HTML файлами в корне проекта
    gulp.watch(['projectVeeva/app/js/common.js', 'app/libs/**/*.js'], gulp.parallel('scripts')); // Наблюдение за главным JS файлом и за библиотеками
});
gulp.task('default', gulp.parallel('css-libs', 'sass','jade', 'scripts',  'scriptsMain','sassMain', 'fontsMain', 'img', 'scriptsSlide' ,'browser-sync', 'watch'));


// gulp.task('build', gulp.parallel('prebuild', 'clean', 'img', 'sass', 'scripts'));
